/**
 * @file
 * Contains definition of the behavior WOW.js.
 */

(function ($, Drupal, drupalSettings, once) {
  "use strict";

  // Load settings from drupalSettings.
  const compat = drupalSettings.wowjs.compat;

  Drupal.behaviors.wowJS = {
    attach: function (context) {
      // Retrieve the elements to be animated.
      const elements = drupalSettings.wowjs.elements;
      const settings = drupalSettings.wowjs.settings;

      console.log(settings);

      $.each(elements, function (index, element) {
        // Define options for WOW.js and ensure defaults for once and mirror.
        let options = {
          selector: element.selector,
          animation: element.animation,
          delay: element.delay,
          time: element.time,
          speed: element.speed,
          duration: element.duration,
          repeat: element.repeat,
          display: element.display,
          clean: element.clean,
          classBox: settings.boxClass,
          offset: element.wow.offset,
          mobile: element.wow.mobile,
          live: element.wow.live,
          once: element.wow.once !== undefined ? element.wow.once : true,
          mirror: element.wow.mirror !== undefined ? element.wow.mirror : false,
          optionalContainer: element.wow.optionalContainer,
          scrollContainer: element.wow.scrollContainer,
        };

        // Ensure WOW.js is initialized only once for each element.
        if (once('wowjs', options.selector).length) {
          new Drupal.WOW(options);
        }
      });

      let container = null;
      if (settings.optionalContainer && settings.scrollContainer !== 'window') {
        container = settings.scrollContainer;
      }

      console.log(`${compat ? '' : 'animate__'}animated`);

      // Customize WOW.js Settings with the specified options.
      let wow = new WOW({
        boxClass: settings.classBox,
        animateClass: `${compat ? '' : 'animate__'}animated`,
        offset: settings.offset,
        mobile: settings.mobile,
        live: settings.live,
        callback: function (box) {
          // Callback function when an animation is started.
        },
        scrollContainer: container,
        resetAnimation: settings.resetAnimation,
      });

      // Initialize WOW.js.
      wow.init();
    }
  };

  /**
   * Drupal.WOW class definition.
   * Initializes WOW.js with the provided options.
   */
  Drupal.WOW = function (options) {
    // Remove previous animation classes if 'clean' is true.
    if (options.clean) {
      $(options.selector).removeClass(function (index, className) {
        return (className.match(/(^|\s)animate__\S+/g) || []).join(' ');
      });
    }

    // Add WOW box class to the selected elements.
    //$(options.selector).addClass(options.classBox);

    let Prefix  = compat ? '' : 'animate__';
    let Classes = `${options.classBox} ${Prefix}${options.animation}`;

    if (options.delay && options.delay != 'custom') {
      Classes += ` ${Prefix}${options.delay}`;
    }
    if (options.speed && options.speed != 'custom' && options.speed != 'medium') {
      Classes += ` ${Prefix}${options.speed}`;
    }
    if (options.repeat && options.repeat != 'repeat-1') {
      Classes += ` ${Prefix}${options.repeat}`;
    }

    // Add custom properties for WOW.js animations.
    if (options.delay === 'custom' && !compat) {
      $(options.selector).css('--animate-delay', options.time);
    }
    if (options.speed === 'custom' && !compat) {
      $(options.selector).css('--animate-duration', options.duration);
    }

    // Add custom properties for Animate.css animations.
    if (options.delay === 'custom') {
      $(options.selector).css({
        '-webkit-animation-delay': `${options.time}ms`,
        '-moz-animation-delay': `${options.time}ms`,
        '-ms-animation-delay': `${options.time}ms`,
        '-o-animation-delay': `${options.time}ms`,
        'animation-delay': `${options.time}ms`,
        '--animate-delay': `${options.time}ms`,
      });
    }
    if (options.speed === 'custom') {
      $(options.selector).css({
        '-webkit-animation-duration': `${options.duration}ms`,
        '-moz-animation-duration': `${options.duration}ms`,
        '-ms-animation-duration': `${options.duration}ms`,
        '-o-animation-duration': `${options.duration}ms`,
        'animation-duration': `${options.duration}ms`,
        '--animate-duration': `${options.duration}ms`,
      });
    }

    // Check and change display property if required.
    if (options.display) {
      var displayValue = $(options.selector).css('display');
      if (displayValue === 'inline') {
        $(options.selector).css('display', 'inline-block');
      }
    }

    Drupal.wowScrollEventListener(options, Prefix, Classes);

    // Add WOW.js classes to the selected elements.
    $(options.selector).addClass(Classes);
  };

  /**
   * Drupal.wowScrollEventListener class definition.
   *
   * Add event listener to manage animation on scroll.
   */
  Drupal.wowScrollEventListener = function (options, Prefix, Classes) {
    let storeClasses = Classes.replace('wow', `${Prefix}animated`);
    // Add event listener to manage animation on scroll.
    $(window).on('scroll', function (e) {
      const scrolled = $(window).scrollTop();
      const winHeight = $(window).height();

      // Show elements in viewport
      $(`${options.selector}`).each(function () {
        const $this = $(this);
        const offsetTop = $this.offset().top;
        const elementHeight = $this.outerHeight();
        const offsetMiddle = offsetTop + (elementHeight / 2);

        $this.removeClass('wow');

        // Check if the middle of the element is in the viewport
        if (scrolled + winHeight > offsetMiddle && scrolled < offsetMiddle) {
          $this.addClass(storeClasses).css('visibility', 'visible').css('animation-name', options.animation);
        } else {
          // Hide elements out of viewport if mirror is true or once is false
          if (options.mirror) {
            setTimeout(function () {
              $this.removeClass(storeClasses).css('visibility', 'hidden').css('animation-name', 'none');
            }, 100); // Adjust the timeout duration as needed
          }
        }
      });
    });
  };

})(jQuery, Drupal, drupalSettings, once);
